import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by y_ryu on 2016/11/24.
 */
public class BackgroundTaskTest {

    @Rule
    public Timeout timeout = new Timeout(1000);

    @Test
    public void invokeで別スレッドで実行される() throws Exception {

        // SetUp
        final AtomicReference<String> backgroundThreadName = new AtomicReference<String>();

        final CountDownLatch latch = new CountDownLatch(1);
        Runnable task = new Runnable() {
            public void run() {
                backgroundThreadName.set(Thread.currentThread().getName());
                latch.countDown();
            }
        };
        BackgroundTask sut = new BackgroundTask(task);

        // Exercise
        sut.invoke();
        latch.await();

        //Verify
        assertThat(backgroundThreadName.get(), is(not(Thread.currentThread().getName())));
    }
}
