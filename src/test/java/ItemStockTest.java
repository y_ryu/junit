import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by y_ryu on 2016/11/24.
 */
@RunWith(Enclosed.class)
public class ItemStockTest {

    public static class 初期状態の場合 {

        private ItemStock sut = new ItemStock();
        private Item book = new Item("book", 100);

        @Test
        public void getNumで0が取得できる() throws Exception {

            int actual = sut.getNum(book);
            int expected = 0;

            assertThat(actual, is(expected));
        }

        @Test
        public void addでItemを追加するとgetNumで1が取得できる() throws Exception {

            sut.add(book);

            int actual = sut.getNum(book);
            int expected = 1;

            assertThat(actual, is(expected));
        }
    }


    public static class itemが1つ追加されている状態 {

        private ItemStock sut = new ItemStock();
        private Item book = new Item("book", 100);
        private Item apple = new Item("apple", 100);

        @Before
        public void setUp() {

            sut.add(book);
        }

        @Test
        public void getNumで1が取得できる() throws Exception {

            int actual = sut.getNum(book);
            int expected = 1;

            assertThat(actual, is(expected));
        }

        @Test
        public void addで同じオブジェクトを追加するとgetNumで2が取得できる() throws Exception {

            sut.add(book);
            int actual = sut.getNum(book);
            int expected = 2;

            assertThat(actual, is(expected));
        }

        @Test
        public void addで異なるオブジェクトを追加するとgetNumで1が取得できる() throws Exception {

            sut.add(apple);
            int actual = sut.getNum(book);
            int expected = 1;

            assertThat(actual, is(expected));
        }
    }

}
