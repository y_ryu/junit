import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

/**
 * Created by y_ryu on 2016/11/24.
 */
public class UtilTest {

    @Test
    public void aaaを入力するとaaaが取得できる() throws Exception {

        String actual = Util.toSnakeCase("aaa");
        String expected = "aaa";

        Assert.assertThat(actual, is(expected));
    }


    @Test
    public void HelloWorldを入力するとhello_worldが取得できる() throws Exception {

        String actual = Util.toSnakeCase("HelloWorld");
        String expected = "hello_world";

        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void PracticeJunitを入力するとpractice_junitが取得できる() throws Exception {

        String actual = Util.toSnakeCase("PracticeJunit");
        String expected = "practice_junit";

        Assert.assertThat(actual, is(expected));
    }
}
