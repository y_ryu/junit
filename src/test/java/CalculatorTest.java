import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

/**
 * Created by y_ryu on 2016/11/24.
 */
public class CalculatorTest {

    @Test(expected=IllegalArgumentException.class)
    public void divideの第二引数に0を渡すとIllegalArgumentExceptionが発生する() throws Exception {

        Calculator.divide(3, 0);
    }
}
