import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by y_ryu on 2016/11/24.
 */
@RunWith(Enclosed.class)
public class CounterTest {

    public static class 初期状態の場合 {

        private Counter sut = new Counter();

        @Test
        public void incrementを実行すると1が取得できる() throws Exception {

            int actual = sut.increment();
            int expected = 1;

            assertThat(actual, is(expected));
        }
    }


    public static class incrementが一回実行された場合 {

        private Counter sut = new Counter();

        @Before
        public void setUp() {

            sut.increment();
        }

        @Test
        public void incrementを実行すると2が取得できる() throws Exception {

            int actual = sut.increment();
            int expected = 2;

            assertThat(actual, is(expected));
        }
    }

    public static class incrementが50回実行された場合 {

        private Counter sut = new Counter();

        @Before
        public void setUp() {
            for (int i = 0; i < 50; i++) {
                sut.increment();
            }
        }

        @Test
        public void incrementを実行すると51が取得できる() throws Exception {

            int actual = sut.increment();
            int expected = 51;

            assertThat(actual, is(expected));
        }
    }
}
