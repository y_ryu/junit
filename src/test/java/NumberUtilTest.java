import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

/**
 * Created by y_ryu on 2016/11/24.
 */
public class NumberUtilTest {

    @Test
    public void isEvenに10を入力するとtrueが取得できる() throws Exception {

        boolean actual = NumberUtil.isEven(10);
        boolean expected = true;

        Assert.assertThat(actual, is(expected));
    }


    @Test
    public void isEvenに7を入力するとfalseが取得できる() throws Exception {

        boolean actual = NumberUtil.isEven(7);
        boolean expected = false;

        Assert.assertThat(actual, is(expected));
    }
}
