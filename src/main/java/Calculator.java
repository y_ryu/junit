/**
 * Created by y_ryu on 2016/11/24.
 */
public final class Calculator {

    public static int divide(int x, int y) {

        if (y == 0) throw new IllegalArgumentException("0による除算です");
        return x / y;
    }
}
