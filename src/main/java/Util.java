import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by y_ryu on 2016/11/24.
 */
public final class Util {

    private Util() {
        throw new AssertionError("インスタンス化不可のユーティリティクラスです。");
    }

    public static String toSnakeCase(String param) {

        Pattern p = Pattern.compile("[A-Z]");

        for(;;) {

            Matcher m = p.matcher(param);
            if(!m.find()) break;
            param = m.replaceFirst("_" + m.group().toLowerCase());
        }

        return param.replaceFirst("^_", "");
    }
}
