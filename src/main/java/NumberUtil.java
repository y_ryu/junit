import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by y_ryu on 2016/11/24.
 */
public final class NumberUtil {

    private NumberUtil() {
        throw new AssertionError("インスタンス化不可のユーティリティクラスです。");
    }

    public static boolean isEven(int param) {

        return (param % 2) == 0;
    }
}
