import java.util.ArrayList;
import java.util.List;

/**
 * Created by y_ryu on 2016/11/24.
 */
public class FizzBuzz {

    public static List<String> createFizzBuzz(int size) {

        List<String> list = new ArrayList<String>(size);

        for(int i = 1; i <= size; i++) {

            if (i % 15 == 0) {
                list.add("FizzBuzz");
            } else if (i % 5 == 0) {
                list.add("Fizz");
            } else if (i % 3 == 0) {
                list.add("Buzz");
            } else {
                list.add(String.valueOf(i));
            }
        }
        return list;
    }
}
