/**
 * Created by y_ryu on 2016/11/24.
 */
public class Item {

    public final String name;
    public final int price;

    public Item(String name, int price) {
        this.name = name;
        this.price = price;
    }

}
