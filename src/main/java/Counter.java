/**
 * Created by y_ryu on 2016/11/24.
 */
public class Counter {

    private int counter;

    public int increment() {
        return ++counter;
    }
}
