import java.util.concurrent.Executors;

/**
 * Created by y_ryu on 2016/11/24.
 */
public class BackgroundTask {

    private final Runnable task;

    BackgroundTask(Runnable task) {
        this.task = task;
    }

    public void invoke() {
        Executors.newSingleThreadExecutor().execute(task);
    }


}
